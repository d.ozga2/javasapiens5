package pl.sda.main;

import pl.sda.objects.Dog;

public class DogMain {

    public static void main(String[] args) {

        Dog dog1 = new Dog("Owczarek", "Reksio", 12.4, 3);


        dog1.setWeight(-11.4);

        dog1.bark();

        System.out.println("Rasa psa: " + dog1.getBreed());
        System.out.println("Waga psa: " + dog1.getWeight());

    }

}
