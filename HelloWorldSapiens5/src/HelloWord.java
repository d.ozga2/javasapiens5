public class HelloWord {

    // ctrl + / - komentarz zwykły //
    // ctrl + Shift + / = komentarz blokowy /*

    public static void main(String[] args) {
        System.out.println("Hello World!");
        // sout
        // \n - znak specjalny oznaczający przejście do nowej linii
        System.out.print("Hello from my app!\n");

        System.out.print("Third line");
    }






}
