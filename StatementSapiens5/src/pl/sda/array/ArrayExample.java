package pl.sda.array;

public class ArrayExample {

    public static void main(String[] args) {

        int [] array = new int[5]; //tablica uzupełniona zerami

        int array2[] = new int[5]; //alternatywny zapis tworzenia tablicy

//        System.out.println( array[-1] ); // nieprawidłowy indeks tablicy - program przestanie działać

        array[0] = 4;
        array[1] = 6;
        array[2] = 66;
        array[3] = 55;
        array[4] = 13;

        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }

        String [] strArray = {"Ala", "ma", "kota"};

        String [] longerStrArray = new String[6];

        //kopiowanie zawartości tablic
        for (int i=0; i < strArray.length; i++) {
            longerStrArray[i] = strArray[i];
        }

        //Wypisz wszystkie elementy tablicy array i określ ich parzystość

        for (int i=0; i < array.length; i++) {

            if (array[i] % 2 == 0) {
                System.out.println("Liczba " + array[i] + " jest parzysta");
            } else {
                System.out.println("Liczba " + array[i] + " jest nieparzysta");
            }
        }
    }
}
