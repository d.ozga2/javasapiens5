package pl.sda.array;

import java.util.Arrays;

public class Array2DExample {

    public static void main(String[] args) {

        int [][] array2d = new int[20][20];

        for (int i = 0; i < array2d.length; i++) {

            for (int j = 0; j < array2d[i].length; j++) {

                array2d[i][j] = (i + 1) * (j + 1);
            }
        }

        for (int i = 0; i < array2d.length; i++) {
            for (int j = 0; j < array2d[i].length; j++) {
                System.out.print(array2d[i][j] + "\t");
            }
            System.out.println(); //przejście do nowej linii
        }

        for (int i = 0; i < array2d.length; i++) {
            System.out.print(array2d[5][i] + "\t");
        }

        System.out.println();

        System.out.println(Arrays.toString(array2d[5]).replaceAll(",", ""));


        String [][] strArray2D = { {"Ala", "ma", "kota"}, {"A", "kot", "ma", "Alę"} };

        for (int i = 0; i < strArray2D.length; i++) {

            for (int j = 0; j < strArray2D[i].length; j++) {
                System.out.print(strArray2D[i][j] + " ");
            }

            System.out.println();

        }

    }



}
