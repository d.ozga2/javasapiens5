package pl.sda.array;

import java.util.Scanner;

public class SearchInArrayExample {

    public static void main(String[] args) {

        int [] array = {4,5213, 213, 232, 32432,11,4,-3434, -5656};

        Scanner sc = new Scanner(System.in);

        int input;

        boolean found;

        do {
            found = false;
            System.out.println("Podaj liczbę, zero kończy program: ");

            input = sc.nextInt();

            if (input == 0) {
                break;
            }

            for (int element : array) {
                if (element == input) {
                    System.out.println("Liczba " + input + " znajduje się w zbiorze");
                    found = true;
                    break; //przerywa działanie for
                }
            }

            if (!found) {
                System.out.println("Nie znaleziono liczby");
            }

        } while (true);

    }

}
