package pl.sda.loop;

public class WhileExample {

    //main
    //psvm

    public static void main(String[] args) throws InterruptedException {

        boolean flag = true;
        int iteration = 0;

        while (flag) { //dopóki flag ma wartość true

            System.out.print(iteration++); //iteration++ -> iteration = interation + 1

            if (iteration == 5) {
                flag = false;
            }

            System.out.print("x");

            Thread.sleep(2000);
        }
    }
}
