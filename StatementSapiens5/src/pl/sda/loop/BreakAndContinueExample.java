package pl.sda.loop;

public class BreakAndContinueExample {

    public static void main(String[] args) {

        for (int i = 0; i < 10; i++) {

            System.out.print(i);

            if (i == 3) {
                continue; //przerywa bieżący obrót pętli
            }

            if (i == 5) {
                break;
            }

            System.out.print("x");

        }

    }

}
