package pl.sda.loop;

import java.util.Random;
import java.util.Scanner;

public class DoWhileExample {

    public static void main(String[] args) {

        Random r = new Random();

        int randomNumber = r.nextInt(11); //losuje liczby od 0 do 10

        Scanner sc = new Scanner(System.in);

        int input;

        do {

            System.out.println("Podaj liczbę całkowitą z przedziału 0 - 10:");
            input = sc.nextInt();

        } while (randomNumber != input);

        System.out.println("Brawo zgadłeś!");

    }

}
