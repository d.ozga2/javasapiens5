package pl.sda.loop;

public class ForEachExample {

    public static void main(String[] args) {

        int [] array = {1, 2, 3, 4, 5};

        for (int element : array) {
            System.out.print(element + " ");

            element = element * 3;

        }

        System.out.println("\n Po aktualizacji");


        for (int element : array) {
            System.out.print(element + " ");
        }

    }

}
